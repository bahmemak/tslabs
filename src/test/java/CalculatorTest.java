import Calculator.Calculator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CalculatorTest {
    static Calculator c;
    @Before
    public void initVariable() {
        c = new Calculator();
    }

    @Test
    public void add_twoTwo_four() {
        int a = 2;
        int b = 2;
        int expectedResult = 4;

        int result = c.add(a, b);

        assertEquals(expectedResult, result);
    }

    @Test
    public void subtract_tenFour_six() {
        int a = 10;
        int b = 4;
        int expectedResult = 6;

        int result = c.subtract(a, b);

        assertEquals(expectedResult, result);
    }

    @Test
    public void multiply_threeThree_nine() {
        int a = 3;
        int b = 3;
        int expectedResult = 9;

        int result = c.multiply(a, b);

        assertEquals(expectedResult, result);
    }

    @Test
    public void divide_tenFive_two() {
        int a = 10;
        int b = 5;
        int expectedResult = 2;

        int result = c.divide(a, b);

        assertEquals(expectedResult, result);
    }

    @Test
    public void divide_byZero_throwsException() {
        int a = 1;
        int b = 0;
        Exception exception = assertThrows(ArithmeticException.class, () -> c.divide(a, b));

        String expectedMessage = "/ by zero";
        String actualMessage = exception.getMessage();

        assertEquals(expectedMessage, actualMessage);
    }

}


